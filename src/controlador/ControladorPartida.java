/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.interfaces.IVistaPartida;
import java.util.Observable;
import java.util.Observer;
import modelo.Fachada;
import modelo.Partida;
import modelo.Tile;
import modelo.Usuario;

/**
 *
 * @author Gabriel
 */
public class ControladorPartida implements Observer{
    
    private Fachada modelo = Fachada.getInstancia();
    private IVistaPartida vista;
    private Usuario jugador;
    private Partida partida;

    public ControladorPartida(IVistaPartida vista, Usuario jugador, Partida partida) {
        this.vista = vista;
        this.jugador = jugador;
        this.partida = partida;
        
        this.modelo.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.vista.dibujarPartida(this.partida);
        this.vista.actualizarInformacionPartida();
                
    }

    public void marcarTile(Tile tile) {
        this.partida.marcarTile(tile, this.jugador);
        
    }
    
    
    
}
