/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.interfaces;

import modelo.Partida;

/**
 *
 * @author Vic
 */
public interface IVistaPartida {

    public void dibujarPartida(Partida partida);

    public void actualizarInformacionPartida();

}
