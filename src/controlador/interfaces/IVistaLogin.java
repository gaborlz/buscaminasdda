/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.interfaces;

import modelo.Usuario;

/**
 *
 * @author Vic
 */
public interface IVistaLogin {

    public void login();

    public void loginCorrecto(Usuario pUsuario);

    public void loginIncorrecto(String pMsg);
}
