/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.interfaces.IVistaLogin;
import java.util.ArrayList;
import modelo.Fachada;
import modelo.Partida;
import modelo.Usuario;
import vista.VistaSeteoJuego;
import vista.VistaPartida;

/**
 *
 * @author Vic
 */
public class ControladorLogin {

    private Fachada modelo = Fachada.getInstancia();
    private IVistaLogin vista;

    public ControladorLogin(IVistaLogin vista) {
        this.vista = vista;
    }

    public void login(String username, String password) {
        Usuario usr = modelo.login(username, password);
        if (usr == null) {
            this.vista.loginIncorrecto("Los datos ingresador no son correctos, por favor verifique.");
        } else {
            this.vista.loginCorrecto(usr);
        }
    }

    public void entrarAJugar(Usuario pUsuario) {
        ArrayList<Partida> vPartidas = this.modelo.getPartidasPendientes();
        if (vPartidas != null && vPartidas.size() > 0) {
            vPartidas.get(0).agregarJugador(pUsuario);
            VistaPartida vistaPartida = new VistaPartida(vPartidas.get(0), pUsuario);
            vistaPartida.setVisible(true);
        } else {
            VistaSeteoJuego nuevaPartida = new VistaSeteoJuego(null, false, pUsuario);
            nuevaPartida.setVisible(true);
        }
    }
}
