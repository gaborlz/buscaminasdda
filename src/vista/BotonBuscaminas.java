/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import javax.swing.JButton;
import modelo.Tile;

/**
 *
 * @author Vic
 */
public class BotonBuscaminas extends JButton {

    private Tile tile;

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public BotonBuscaminas(Tile tile) {
        this.tile = tile;
    }

}
