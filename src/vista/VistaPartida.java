/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.ControladorPartida;
import controlador.interfaces.IVistaPartida;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JSplitPane;
import modelo.Partida;
import modelo.Usuario;

/**
 *
 * @author Vic
 */
public class VistaPartida extends javax.swing.JFrame implements IVistaPartida, ActionListener {

    /**
     * Creates new form VistaPartida
     */
    private JSplitPane split = new JSplitPane();

    private ControladorPartida controlador;
    private Usuario jugador;
    private Partida partida;
    private PanelInfoBuscaminas panelInformacion;

    public VistaPartida(Partida partida, Usuario jugador) {
        initComponents();
        this.controlador = new ControladorPartida(this, jugador, partida);
        this.jugador = jugador;
        this.partida = partida;
        this.panelInformacion = new PanelInfoBuscaminas(jugador, this);
        this.split.setTopComponent(this.panelInformacion);

        setContentPane(this.split);

        this.split.setOrientation(JSplitPane.VERTICAL_SPLIT);
        this.split.setDividerLocation(120);
        this.dibujarPartida(partida);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
     * content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 570, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void dibujarPartida(Partida partida) {
        this.split.setBottomComponent(new PanelBuscaminas(partida, this));
        this.validate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        BotonBuscaminas boton = (BotonBuscaminas) e.getSource();
        this.controlador.marcarTile(boton.getTile());
    }

    @Override
    public void actualizarInformacionPartida() {

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
