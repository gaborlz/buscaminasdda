/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Vic
 */
public class Partida {

    private ArrayList<Usuario> jugadores = new ArrayList();

    private ArrayList<Tile> tiles = new ArrayList();

    private Usuario turnoActual;

    private ArrayList<Color> coloresUsados = new ArrayList();

    public void marcarTile(Tile tile, Usuario jugador) {
        if (jugador.equals(this.turnoActual)) {
            tile.setUsuarioQueSelecciono(jugador);
            for (Usuario u : this.jugadores) {
                if (!u.equals(jugador)) {
                    this.turnoActual = u;
                }
            }
            this.nuevaMina();
            Fachada.getInstancia().avisar(this);
        }

    }

    private int dimension;

    private int apuestaInicial;

    public Partida(int dimension, int apuestaInicial, Usuario jugador) {
        this.dimension = dimension;
        this.apuestaInicial = apuestaInicial;
        this.agregarJugador(jugador);
        this.turnoActual = jugador;
        this.instalarPartida();
    }

    public void agregarTile(Tile tile) {
        this.tiles.add(tile);
    }

    public ArrayList<Usuario> getJugadores() {
        return jugadores;
    }

    public void setJugadores(ArrayList<Usuario> jugadores) {
        this.jugadores = jugadores;
    }

    public ArrayList<Tile> getTiles() {
        return tiles;
    }

    public void setTiles(ArrayList<Tile> tiles) {
        this.tiles = tiles;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getApuestaInicial() {
        return apuestaInicial;
    }

    public void setApuestaInicial(int apuestaInicial) {
        this.apuestaInicial = apuestaInicial;
    }

    public void agregarJugador(Usuario pUsuario) {
        if (!this.coloresUsados.contains(Color.BLUE)) {
            pUsuario.setColor(Color.BLUE);
            this.coloresUsados.add(Color.BLUE);
        } else {
            pUsuario.setColor(Color.ORANGE);
            this.coloresUsados.add(Color.ORANGE);
        }
        jugadores.add(pUsuario);
    }

    private void nuevaMina() {
        int Low = 0;
        int High = (this.dimension * this.dimension) - 1;
        boolean pudeCrearNuevaMina = false;
        while (!pudeCrearNuevaMina) {
            Random r = new Random();
            int valor = r.nextInt(High - Low) + Low;
            if (this.tiles.get(valor).getUsuarioQueSelecciono() == null) {
                this.tiles.get(valor).setTieneMina(true);
                pudeCrearNuevaMina = true;
            }
        }
    }

    private void instalarPartida() {
        for (int i = 0; i < this.dimension * this.dimension; i++) {
            Tile t = new Tile();
            this.agregarTile(t);
        }
        this.nuevaMina();
    }

}
