/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Vic
 */
public class Tile {

    private boolean tieneMina;
    private Usuario usuarioQueSelecciono;

    public boolean isTieneMina() {
        return tieneMina;
    }

    public void setTieneMina(boolean tieneMina) {
        this.tieneMina = tieneMina;
    }

    public Usuario getUsuarioQueSelecciono() {
        return usuarioQueSelecciono;
    }

    public void setUsuarioQueSelecciono(Usuario usuarioQueSelecciono) {
        this.usuarioQueSelecciono = usuarioQueSelecciono;
    }

}
