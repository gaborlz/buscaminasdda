/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author Vic
 */
public class SistemaUsuarios {

    private ArrayList<Usuario> usuarios = new ArrayList();

    public void agregar(Usuario user) {
        this.usuarios.add(user);
    }

    public Usuario login(String username, String password) {
        for (Usuario user : this.usuarios) {
            if (user.getUsername().equalsIgnoreCase(username) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public ArrayList<Usuario> getListaUsuarios() {
        return this.usuarios;
    }

    public void EliminarUsuario(Usuario pUsuario) {
        this.usuarios.remove(pUsuario);
    }

    public void CargarUsuarios() {
        Usuario user;

        user = new Usuario("pepe", "pepe", "Pepe", 100, false);
        this.agregar(user);

        user = new Usuario("jon", "jon", "Jon Snow", 200, false);
        this.agregar(user);

        user = new Usuario("magaxine", "magaxine", "b", 30, true);
        this.agregar(user);

        user = new Usuario("maria", "maria", "Maria", 10, false);
        this.agregar(user);

    }
}
