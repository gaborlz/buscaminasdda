/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author Vic
 */
public class Fachada extends Observable {

    private static Fachada instancia = new Fachada();
    private SistemaUsuarios sistemaUsuarios;
    private SistemaPartidas sistemaPartidas;

    private Fachada() {
        this.sistemaUsuarios = new SistemaUsuarios();
        this.sistemaPartidas = new SistemaPartidas();
        this.sistemaUsuarios.CargarUsuarios();
        //this.sistemaJuegos = new SistemaJuegos();
    }

    public static Fachada getInstancia() {
        return Fachada.instancia;
    }

    protected void avisar(Object pObj) {
        this.setChanged();
        notifyObservers(pObj);
    }

    public Usuario login(String pNombre, String pPassword) {
        return this.sistemaUsuarios.login(pNombre, pPassword);
    }

    public ArrayList<Partida> getPartidasPendientes() {
        return this.sistemaPartidas.getPartidasPendientes();
    }

    public void nuevaPartida(Partida partida) {
        this.sistemaPartidas.nuevaPartida(partida);
    }

}
