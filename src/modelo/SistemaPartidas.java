/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author Vic
 */
public class SistemaPartidas {

    private ArrayList<Partida> partidas = new ArrayList();

    public void nuevaPartida(Partida partida) {
        this.partidas.add(partida);
    }

    public void eliminarPartida(Partida partida) {
        this.partidas.remove(partida);
    }

    public ArrayList<Partida> getPartidas() {
        return partidas;
    }

    ArrayList<Partida> getPartidasPendientes() {
        ArrayList<Partida> retorno = new ArrayList();
        for (Partida p : this.partidas) {
            if (p.getJugadores().size() == 1) {
                retorno.add(p);
            }
        }
        return retorno.size() > 0 ? retorno : null;
    }

}
