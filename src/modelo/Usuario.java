/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.awt.Color;

/**
 *
 * @author Vic
 */
public class Usuario {

    private String nombre;
    private String username;
    private String password;
    private int saldo;
    private boolean esAdmin;
    private Color color;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public boolean getEsAdmin() {
        return esAdmin;
    }

    public void setEsAdmin(boolean esAdmin) {
        this.esAdmin = esAdmin;
    }

    public Usuario(String username, String password, String nombre, int saldo, boolean esAdmin) {
        this.username = username;
        this.password = password;
        this.nombre = nombre;
        this.saldo = saldo;
        this.esAdmin = esAdmin;
    }

    @Override
    public String toString() {
        return this.username + this.nombre + this.saldo + this.esAdmin;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getUsername() == ((Usuario) obj).getUsername();
    }
}
