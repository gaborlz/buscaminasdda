/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author Vic
 */
public class Buscaminas {

    private ArrayList<Partida> partidas = new ArrayList();

    public ArrayList<Partida> getPartidas() {
        return partidas;
    }

    public void nuevaPartida(Partida pPartida) {
        this.partidas.add(pPartida);
    }

    public void cerrarPartida(Partida pPartida) {
        this.partidas.remove(pPartida);
    }
}
